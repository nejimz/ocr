<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$this->get('/', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login')->name('login_auth');

Route::prefix('portal')
->middleware('auth')
->group(function(){
	$this->post('logout', 'Auth\LoginController@logout')->name('logout');

	Route::get('users/reset-password', 'UsersController@reset_password_form')->name('reset_password_form');
	Route::post('users/reset-password', 'UsersController@reset_password')->name('reset_password');

	Route::get('/', 'HomeController@index')->name('home');
	Route::get('drivers/search', 'DriverController@json_search')->name('json_search');
	Route::resource('drivers', 'DriverController');
	Route::resource('vehicles', 'VehicleController');

	Route::prefix('settings')
	->middleware('AdminAccess')
	->group(function(){
		Route::resource('violation-type', 'ViolationTypeController');
		Route::resource('vehicle-body-type', 'VehicleBodyTypeController');
		Route::resource('fuel', 'FuelController');

		Route::get('users/generate-password', 'UsersController@generate_password')->name('generate_password');
		Route::resource('users', 'UsersController');
	});
	
	Route::get('violations/upload', 'ViolationsController@upload_form')->name('violation_upload');
	Route::post('violations/upload', 'ViolationsController@process_image')->name('violation_process');
	Route::resource('violations', 'ViolationsController');

	Route::prefix('reports')
	->group(function(){
		Route::resource('report-violation', 'ReportViolationController');
	});
});
