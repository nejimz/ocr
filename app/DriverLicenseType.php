<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverLicenseType extends Model
{
     public $table = 'driver_license_type';
     public $timestamps = true;

     protected $fillable = ['name'];

     public function driver()
     {
     	return $this->belongsTo('App\Driver', 'id', 'license_type_id');
     }
}
