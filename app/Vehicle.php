<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    public $table = 'vehicle';
    public $timestamps = true;

    protected $fillable = ['mv_file_number', 'plate_number', 'engine_number', 'chasis_number', 'denomination', 'number_of_cylinders', 'fuel_id', 'make', 'series', 'body_type_id', 'body_number', 'year_model', 'driver_name', 'driver_id', 'created_at', 'updated_at'];

    public function fuel()
    {
    	return $this->hasOne('App\VehicleFuel', 'id', 'fuel_id');
    }

    public function body_type()
    {
    	return $this->hasOne('App\VehicleBodyType', 'id', 'body_type_id');
    }

    public function driver()
    {
    	return $this->hasOne('App\Driver', 'id', 'driver_id');
    }

    public function violation()
    {
        return $this->belongsTo('App\Violation', 'id', 'vehicle_id');
    }
}
