<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required', 
            'email' => 'required|email', 
            'access' => 'required', 
            'active' => 'required'
        ];
    }

     public function attributes()
    {
        return [
            'name' => 'Name', 
            'email' => 'Email', 
            'access' => 'Access', 
            'active' => 'Active'
        ];
    }

    public function messages()
    {
        return [
            'required'  => ':attribute is required.',
            'email'     => ':attribute is not an email.',
            'unique'    => ':attribute already exists.'
        ];
    }
}
