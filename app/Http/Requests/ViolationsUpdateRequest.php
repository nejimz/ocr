<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ViolationsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'license_number' => 'required|exists:driver,license_number', 
            'plate_number' => 'required|exists:vehicle,plate_number', 
            'ticket_date' => 'required', 
            'remarks' => 'required', 
            'violation_list' => 'required'
        ];
    }

     public function attributes()
    {
        return [
            'ticket_date' => 'Ticket Date', 
            'remarks' => 'Remarks', 
            'driver_name' => 'Driver Name', 
            'license_number' => 'License Number', 
            'plate_number' => 'Plate Number', 
            'violation_list' => 'Violation'
        ];
    }

    public function messages()
    {
        return [
            'required'  => ':attribute is required.',
            'email'     => ':attribute is not an email.',
            'exists'    => ':attribute does not exists.',
            'unique'    => ':attribute already exists.'
        ];
    }
}
