<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VehicleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mv_file_number' => 'required|unique:driver,license_number', 
            'plate_number' => 'required', 
            'engine_number' => 'required', 
            'chasis_number' => 'required', 
            'denomination' => 'required', 
            'number_of_cylinders' => 'required', 
            'fuel_id' => 'required', 
            'make' => 'required', 
            'series' => 'required', 
            'body_type_id' => 'required', 
            'body_number' => 'required', 
            'year_model' => 'required', 
            'license_number' => 'required'
        ];
    }

     public function attributes()
    {
        return [
            'mv_file_number' => 'MV File Number', 
            'plate_number' => 'Plate Number', 
            'engine_number' => 'Engine Number', 
            'chasis_number' => 'Chasis Number', 
            'denomination' => 'Denomination', 
            'number_of_cylinders' => 'Number of Cylinders', 
            'fuel_id' => 'Fuel', 
            'make' => 'Make', 
            'series' => 'Series', 
            'body_type_id' => 'Body Type', 
            'body_number' => 'Body Number', 
            'year_model' => 'Year Model', 
            'license_number' => 'License Number'
        ];
    }

    public function messages()
    {
        return [
            'required'  => ':attribute is required.',
            'email'     => ':attribute is not an email.',
            'unique'    => ':attribute already exists.'
        ];
    }
}
