<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Auth;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $search = '';

        if($request->has('search'))
        {
            $search = $request->search;

        }

        $rows = User::where('name', 'LIKE', "$search%")->orWhere('email', 'LIKE', "$search%")->paginate(100);

        $data = compact('rows', 'search');

        return view('user.index', $data);
    }
    
    public function create()
    {
    	$name = old('name');
    	$email = old('email');
    	$access = old('access');
    	$active = old('active');
    	$route = route('users.store');

    	$data = compact('route', 'name', 'email', 'access', 'active');

        return view('user.create-form', $data);
    }
    
    public function store(Requests\UsersStoreRequest $request)
    {
        #dd($request->all());
        $input = $request->except('_token');
        $input['password'] = bcrypt('p@ssw0rd');

        User::create($input);
        
        return redirect()->route('users.create')->with('success', 'User Successfully added!');
    }
    
    public function edit($id)
    {
    	$route = route('users.update', $id);
        $row = User::whereId($id)->first();
    	$name = $row->name;
    	$email = $row->email;
    	$access = $row->access;
    	$active = $row->active;

    	$data = compact('route', 'name', 'email', 'access', 'active');

        return view('user.create-form', $data);
    }
    
    public function update($id, Request $request)
    {
        #dd($request->all());   
        $input = $request->except('_token', '_method', 'password');

        User::whereId($id)->update($input);

        return redirect()->route('users.edit', $id)->with('success', 'User Successfully updated!');
    }
    
    public function show($id)
    {
        $row = User::whereId($id)->first();
        $name = $row->name;
        $email = $row->email;
        $access = $row->access;
        $active = $row->active;

        $data = compact('name', 'email', 'access', 'active');

        return view('user.show', $data);
    }

    public function generate_password(Request $request)
    {
        $password = str_random(10);

        User::whereId($request->id)->update([ 'password'=>bcrypt($password) ]);

        return response()->json([ 'password'=>$password ]);
    }

    public function reset_password_form()
    {
        $route = route('reset_password');

        $data = compact('route');

        return view('user.reset', $data);
    }

    public function reset_password(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ]);

        $password = bcrypt($request->password);

        User::whereId(Auth::user()->id)->update([ 'password'=>$password ]);

        return redirect()->route('reset_password_form')->with('success', 'Password Successfully changed!');
    }
}
