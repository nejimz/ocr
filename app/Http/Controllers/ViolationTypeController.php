<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ViolationType;

class ViolationTypeController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        $query = new ViolationType;

        if($request->has('search'))
        {
            $search = $request->search;
            $query = $query->where('name', 'LIKE', "$search%");
        }

        $rows = $query->orderBy('name', 'ASC')->paginate(100);

        $data = compact('rows', 'search');

        return view('settings.violation-type.index', $data);
    }
    
    public function create()
    {
    	$route = route('violation-type.store');
        $name = old('name');
        $price = old('price');

    	$data = compact('route', 'name', 'price');

        return view('settings.violation-type.create-form', $data);
    }
    
    public function store(Request $request)
    {
    	$request->validate([
    		'name' => 'required|unique:violation_type,name',
            'price' => 'required|regex:/^\d{1,13}(\.\d{1,4})?$/'
    	]);

        $input = $request->only('name', 'price');

        ViolationType::create($input);
        
        return redirect()->route('violation-type.create')->with('success', 'Violation Type Successfully added!');
    }
    
    public function edit($id)
    {
    	$route = route('violation-type.update', $id);
        $row = ViolationType::whereId($id)->first();
        $name = $row->name;
        $price = $row->price;

    	$data = compact('route', 'name', 'price');

        return view('settings.violation-type.create-form', $data);
    }
    
    public function update($id, Request $request)
    {
    	$request->validate([
            'name' => 'required|unique:violation_type,name,' . $id . ',id',
            'price' => 'required|regex:/^\d{1,13}(\.\d{1,4})?$/'
    	]);

        $input = $request->only('name', 'price');

        ViolationType::whereId($id)->update($input);

        return redirect()->route('violation-type.edit', $id)->with('success', 'Violation Type Successfully updated!');
    }
    
    public function show($id)
    {
        $row = ViolationType::whereId($id)->first();
        $name = $row->name;
        $price = $row->price;
        
        $data = compact('name', 'price');

        return view('settings.violation-type.show', $data);
    }
}
