<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Driver;
use App\DriverLicenseType;
use App\Http\Requests;
//use Requests\DriverStoreRequest;

class DriverController extends Controller
{
    public function __construct()
    {
        $this->middleware('AdminAccess', ['only' => ['create', 'store', 'edit', 'update']]);
    }
    
    public function index(Request $request)
    {
        $search = '';

        if($request->has('search'))
        {
            $search = $request->search;
        }

        $rows = Driver::where('license_number', 'LIKE', "$search%")
            ->orWhere('first_name', 'LIKE', "$search%")
            ->orWhere('middle_name', 'LIKE', "$search%")
            ->orWhere('last_name', 'LIKE', "$search%")->paginate(100);

        $data = compact('rows', 'search');

        return view('driver.index', $data);
    }
    
    public function create()
    {
        $route = route('drivers.store');

        $id = old('id');
        $license_number = old('license_number');
    	$license_type_id = old('license_type_id');
    	$expiry_date = old('expiry_date');
        $agency_code = old('agency_code');
        $first_name = old('first_name');
        $middle_name = old('middle_name');
        $last_name = old('last_name');
        $birth_date = old('birth_date');
        $gender = old('gender');
        $nationality = old('nationality');
        $height = old('height');
        $weight = old('weight');
        $email = old('email');
        $address = old('address');
        $city = old('city');

        $license_types = DriverLicenseType::orderBy('name', 'ASC')->get();

    	$data = compact('route', 'id', 'license_number', 'license_type_id', 'expiry_date', 'agency_code', 'first_name', 'middle_name', 'last_name', 'birth_date', 'gender', 'nationality', 'height', 'weight', 'email', 'address', 'city', 'license_types');

        return view('driver.create-form', $data);
    }
    
    public function store(Requests\DriverStoreRequest $request)
    {
        #dd($request->all());
        $input = $request->except(['_token']);

        Driver::create($input);

        return redirect()->route('drivers.create')->with('success', 'Driver Details Successfully added!');
    }
    
    public function edit($id)
    {
    	$route = route('drivers.update', $id);
    	$row = Driver::whereId($id)->first();
        $license_number = $row->license_number;
        $license_type_id = $row->license_type_id;
        $expiry_date = $row->expiry_date;
        $agency_code = $row->agency_code;
        $first_name = $row->first_name;
        $middle_name = $row->middle_name;
        $last_name = $row->last_name;
        $birth_date = $row->birth_date;
        $gender = $row->gender;
        $nationality = $row->nationality;
        $height = $row->height;
        $weight = $row->weight;
        $email = $row->email;
        $address = $row->address;
        $city = $row->city;

        $license_types = DriverLicenseType::orderBy('name', 'ASC')->get();

        $data = compact('route', 'id', 'license_number', 'license_type_id', 'expiry_date', 'agency_code', 'first_name', 'middle_name', 'last_name', 'birth_date', 'gender', 'nationality', 'height', 'weight', 'email', 'address', 'city', 'license_types');

        return view('driver.create-form', $data);
    }
    
    public function update($id, Requests\DriverUpdateRequest $request)
    {
        $input = $request->except(['_token', '_method']);

        Driver::whereId($id)->update($input);

        return redirect()->route('drivers.edit', $id)->with('success', 'Driver Details Successfully updated!');
    }
    
    public function show($id)
    {
        $row = Driver::whereId($id)->first();
        $license_number = $row->license_number;
        $license_type = $row->license->name;
        $expiry_date = $row->expiry_date;
        $agency_code = $row->agency_code;
        $first_name = $row->first_name;
        $middle_name = $row->middle_name;
        $last_name = $row->last_name;
        $birth_date = $row->birth_date;
        $gender = $row->gender;
        $nationality = $row->nationality;
        $height = $row->height;
        $weight = $row->weight;
        $email = $row->email;
        $address = $row->address;
        $city = $row->city;

        $data = compact('license_number', 'license_type', 'expiry_date', 'agency_code', 'first_name', 'middle_name', 'last_name', 'birth_date', 'gender', 'nationality', 'height', 'weight', 'email', 'address', 'city');

        return view('driver.show', $data);
    }

    public function json_search(Request $request)
    {
        $search = $request->search;
        $rows = Driver::select(DB::raw("CONCAT(last_name, ', ', first_name, ' ', middle_name) as name"), 'license_number')
                ->where('last_name', 'LIKE', "$search%")
                ->orWhere('first_name', 'LIKE', "$search%")
                ->orWhere('middle_name', 'LIKE', "$search%")
                ->orWhere('license_number', 'LIKE', "$search%")
                ->pluck('name', 'license_number');
                #dd($rows);
        return response()->json($rows);
    }
}
