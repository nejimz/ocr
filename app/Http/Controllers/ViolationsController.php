<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Auth;
use DB;
use Image;
use Storage;
use App\User;
use App\Driver;
use App\Vehicle;
use App\Violations;
use App\ViolationType;
use App\ViolationList;
use \Carbon\Carbon;

class ViolationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('AdminAccess', ['only' => ['create', 'store', 'edit', 'update']]);
    }

    public function index(Request $request)
    {
        /*$pix = public_path('images\index.png');
        $exe = 'C:\Program Files\Tesseract-OCR\tesseract.exe';
        $ocr = new TesseractOCR($pix);
        $ocr->executable($exe);
        $ocr->lang('eng');
        dd($ocr->run());
        return view('violations.index');*/

        $search = '';

        if($request->has('search'))
        {
            $search = $request->search;
        }

        $rows = Violations::with(['driver', 'vehicle'])
                ->where(function($query) use ($search){
                    $query->whereHas('driver', function($query) use ($search){
                        $query->where('first_name', 'LIKE', "$search%")
                            ->orWhere('middle_name', 'LIKE', "$search%")
                            ->orWhere('last_name', 'LIKE', "$search%");
                    });

                })
                ->orWhere(function($query) use ($search){
                    $query->whereHas('vehicle', function($query) use ($search){
                        $query->where('plate_number', 'LIKE', "$search%")
                                ->orWhere('mv_file_number', 'LIKE', "$search%");
                    });
                })
                ->paginate(100);

        $data = compact('rows', 'search');

        return view('violations.index', $data);
    }
    
    public function create(Request $request)
    {
        $route = route('violations.store');
    	$ticket_date = old('ticket_date');
    	$remarks = old('remarks');
        $driver_name = old('driver_name');
        $license_number = old('license_number');
        $plate_number = old('plate_number', $request->capture);
        $violation_list = old('violation_list');
        #dd($violation_list);
        $route_upload = route('violation_process');

        $violations = ViolationType::get();

    	$data = compact('route', 'ticket_date', 'remarks', 'driver_name', 'license_number', 'plate_number', 'violation_list', 'violations', 'route_upload');

        return view('violations.create-form', $data);
    }
    
    public function store(Requests\ViolationsStoreRequest $request)
    {
        $row1 = Driver::whereLicenseNumber($request->license_number)->first();
        $row2 = Vehicle::wherePlateNumber($request->plate_number)->first();
        $n = 0;
        $input_list = [];
        $input = $request->only(['ticket_date', 'remarks']);
        $input['driver_id'] = $row1->id;
        $input['vehicle_id'] = $row2->id;
        $input['user_id'] = Auth::user()->id;
        $now = Carbon::now();

        $row = Violations::create($input);

        foreach ($request->violation_list as $key => $value)
        {
            $vt = ViolationType::whereId($key)->first();
            $input_list[$n] = [
                'violation_id' => $row->id, 
                'violation_type_id' => $key,
                'price' => $vt->price,
                'created_at' => $now, 
                'updated_at' => $now
            ];
            $n++;
        }

        ViolationList::insert($input_list);

        return redirect()->route('violations.create')->with('success', 'Driver violation successfully added!');
    }
    
    public function edit($id)
    {
    	$route = route('violations.update', $id);
        $row = Violations::whereId($id)->first();
        #dd($row->list->type->get());
    	$ticket_date = $row->ticket_date;
    	$remarks = $row->remarks;
    	$driver_name = $row->driver->complete_last_name;
    	$license_number = $row->driver->license_number;
        $plate_number = $row->vehicle->plate_number;
        $violation_list = [];
        foreach ($row->list->whereViolationId($row->id)->get() as $violation)
        {
            $violation_list[$violation->type->id] = $violation->type->id.'`'.$violation->type->name;
        }

        $violations = ViolationType::get();

        $data = compact('route', 'ticket_date', 'remarks', 'driver_name', 'license_number', 'plate_number', 'violation_list', 'violations');

        return view('violations.create-form', $data);
    }
    
    public function update($id, Requests\ViolationsUpdateRequest $request)
    {
        $row1 = Driver::whereLicenseNumber($request->license_number)->first();
        $row2 = Vehicle::wherePlateNumber($request->plate_number)->first();
        $n = 0;
        $input_list = [];
        $input = $request->only(['ticket_date', 'remarks']);
        $input['driver_id'] = $row1->id;
        $input['vehicle_id'] = $row2->id;
        $input['user_id'] = $row2->id;
        $now = Carbon::now();
        Violations::whereId($id)->update($input);

        foreach ($request->violation_list as $key => $value)
        {
            $input_list[$n] = [
                'violation_id' => $id, 'violation_type_id' => $key,
                'price' => $vt->price,
                'created_at' => $now, 'updated_at' => $now
            ];
            $n++;
        }

        ViolationList::whereViolationId($id)->delete();
        ViolationList::insert($input_list);

        return redirect()->route('violations.edit', $id)->with('success', 'Driver violation successfully updated!');
    }
    
    public function show($id)
    {
        $row = Violations::whereId($id)->first();
        $ticket_date = $row->ticket_date;
        $remarks = $row->remarks;
        $driver_name = $row->driver->complete_last_name;
        $license_number = $row->driver->license_number;
        $plate_number = $row->vehicle->plate_number;
        $violation_list = [];
        foreach ($row->list->whereViolationId($row->id)->get() as $violation)
        {
            $violation_list[$violation->type->id] = $violation->type->id.'`'.$violation->type->name;
        }

        $data = compact('ticket_date', 'remarks', 'driver_name', 'license_number', 'plate_number', 'violation_list');

        return view('violations.show', $data);
    }
    
    public function upload_form()
    {
        $route = route('violation_process');

        $data = compact('route');

        return view('violations.upload', $data);
    }

    public function process_image(Request $request)
    {
        $request->validate([
            'photo' => 'required|image'
        ]);

        $file           = $request->photo;
        $extension      = $file->getClientOriginalExtension();
        $size           = $file->getClientSize();
        $file_name      = date('Ymdhis') . '_' . str_random(10) . '.' . $extension;
        $file_location  = public_path('images/captures/' . $file_name);

        $image = Image::make($file->getRealPath());
        #$image->invert();
        $image->greyscale();
        #$image->crop(280, 100, 15, 35);
        /*$image->resize(100, null, function ($constraint){
            $constraint->aspectRatio();
        });*/
        $image->save($file_location);

        $pix = $file_location;
        $exe = '';
        $exe1 = 'C:\Program Files(x64)\Tesseract-OCR\tesseract.exe';
        $exe2 = 'C:\Program Files\Tesseract-OCR\tesseract.exe';

        if(file_exists($exe1))
        {
            $exe = $exe1;
        }
        else
        {
            $exe = $exe2;
        }

        $ocr = new TesseractOCR($pix);
        $ocr->executable($exe);
        $ocr->lang('eng');
        //$ocr->whitelist(range('a', 'z'), range(0, 9), '-_@');
        $capture = strtoupper(trim($ocr->run()));

        /*$capture = str_replace("\n", '', $capture);
        $capture = str_replace("\r", '', $capture);
        $capture = str_replace("\"", '', $capture);
        $capture = str_replace("\'", '', $capture);*/

        #dd($capture);
        //Image::make($file->getRealPath())->resize(190, 190)->save($file_location);
        #$pix = public_path('images\test1.png');
        #echo $capture;exit;
        /*$count = Vehicle::wherePlateNumber($capture)->count();

        if($count <= 0)
        {
            #return redirect()->route('violation_upload')->with('warning', 'Plate Number does not exists!');
            return redirect()->route('violations.create')->with('warning', 'Plate Number does not exists!');
        }*/

        return redirect()->route('violations.create', [ 'capture' => $capture ]);
    }
}
