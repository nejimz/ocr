<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Driver;
use App\Vehicle;
use App\VehicleFuel;
use App\VehicleBodyType;

class VehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('AdminAccess', ['only' => ['create', 'store', 'edit', 'update']]);
    }
    
    public function index(Request $request)
    {
        $search = '';

        if($request->has('search'))
        {
            $search = $request->search;
        }

        $rows = Vehicle::where('plate_number', 'LIKE', "$search%")->orWhere('mv_file_number', 'LIKE', "$search%")->paginate(100);

        $data = compact('rows', 'search');

        return view('vehicle.index', $data);
    }
    
    public function create()
    {
        $route = route('vehicles.store');
        $mv_file_number = old('mv_file_number');
        $plate_number = old('plate_number');
    	$engine_number = old('engine_number');
        $chasis_number = old('chasis_number');
        $denomination = old('denomination');
        $number_of_cylinders = old('number_of_cylinders');
        $fuel_id = old('fuel_id');
        $make = old('make');
        $series = old('series');
        $body_type_id = old('body_type_id');
        $body_number = old('body_number');
        $year_model = old('year_model');
        $driver_name = old('driver_name');
        $license_number = old('license_number');

        $fuels = VehicleFuel::orderBy('name', 'ASC')->get();
        $body_types = VehicleBodyType::orderBy('name', 'ASC')->get();

    	$data = compact('route', 'mv_file_number', 'plate_number', 'engine_number', 'chasis_number', 'denomination', 'number_of_cylinders', 'fuel_id', 'make', 'series', 'body_type_id', 'body_number', 'year_model', 'driver_name', 'license_number', 'fuels', 'body_types');

        return view('vehicle.create-form', $data);
    }
    
    public function store(Requests\VehicleStoreRequest $request)
    {
        #dd($request->all());
        $input = $request->except(['_token', 'driver_name', 'license_number']);
        $row = Driver::whereLicenseNumber($request->license_number)->first();
        $input['driver_id'] = $row->id;
        #dd($input);

        Vehicle::create($input);

        return redirect()->route('vehicles.create')->with('success', 'Vehicle Details Successfully added!');
    }
    
    public function edit($id)
    {
    	$route = route('vehicles.update', $id);
        $row = Vehicle::whereId($id)->first();

        $mv_file_number = $row->mv_file_number;
        $plate_number = $row->plate_number;
        $engine_number = $row->engine_number;
        $chasis_number = $row->chasis_number;
        $denomination = $row->denomination;
        $number_of_cylinders = $row->number_of_cylinders;
        $fuel_id = $row->fuel_id;
        $make = $row->make;
        $series = $row->series;
        $body_type_id = $row->body_type_id;
        $body_number = $row->body_number;
        $year_model = $row->year_model;
        $driver_name = $row->driver->complete_last_name;
        $license_number = $row->driver->license_number;

        $fuels = VehicleFuel::orderBy('name', 'ASC')->get();
        $body_types = VehicleBodyType::orderBy('name', 'ASC')->get();

        $data = compact('route', 'mv_file_number', 'plate_number', 'engine_number', 'chasis_number', 'denomination', 'number_of_cylinders', 'fuel_id', 'make', 'series', 'body_type_id', 'body_number', 'year_model', 'driver_name', 'license_number', 'fuels', 'body_types');

        return view('vehicle.create-form', $data);
    }
    
    public function update($id, Requests\VehicleUpdateRequest $request)
    {
        #dd($request->all());
        $input = $request->except(['_token', '_method', 'driver_name', 'license_number']);
        $row = Driver::whereLicenseNumber($request->license_number)->first();
        $input['driver_id'] = $row->id;
        #dd($input);

        Vehicle::whereId($id)->update($input);

        return redirect()->route('vehicles.edit', $id)->with('success', 'Vehicle Details Successfully updated!');
    }
    
    public function show($id)
    {
        $row = Vehicle::whereId($id)->first();

        $mv_file_number = $row->mv_file_number;
        $plate_number = $row->plate_number;
        $engine_number = $row->engine_number;
        $chasis_number = $row->chasis_number;
        $denomination = $row->denomination;
        $number_of_cylinders = $row->number_of_cylinders;
        $fuel = $row->fuel->name;
        $make = $row->make;
        $series = $row->series;
        $body_type_id = $row->body_type_id;
        $body_number = $row->body_number;
        $year_model = $row->year_model;
        $driver_name = $row->driver->complete_last_name;
        $license_number = $row->driver->license_number;

        $data = compact('mv_file_number', 'plate_number', 'engine_number', 'chasis_number', 'denomination', 'number_of_cylinders', 'fuel', 'make', 'series', 'body_type_id', 'body_number', 'year_model', 'driver_name', 'license_number');

        return view('vehicle.show', $data);
    }
}
