<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use App\Vehicle;
use App\Violations;
use App\ViolationType;
use App\ViolationList;
use \Carbon\Carbon;

class ReportViolationController extends Controller
{
    public function create()
    {
    	$ticket_date = old('ticket_date');
    	$route = route('report-violation.index');

    	$data = compact('route', 'ticket_date');

        return view('report.driver-violation', $data);
    }

    public function index(Request $request)
    {
    	$ticket_date = Carbon::parse($request->ticket_date);

    	$rows = Violations::whereTicketDate($ticket_date->toDateString())->get();

    	$data = compact('rows', 'ticket_date');

        return view('report.driver-violation-print', $data);
    }
}
