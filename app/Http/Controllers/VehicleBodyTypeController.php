<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VehicleBodyType;

class VehicleBodyTypeController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        $query = new VehicleBodyType;

        if($request->has('search'))
        {
            $search = $request->search;
            $query = $query->where('name', 'LIKE', "$search%");
        }

        $rows = $query->orderBy('name', 'ASC')->paginate(100);

        $data = compact('rows', 'search');

        return view('settings.vehicle-body-type.index', $data);
    }
    
    public function create()
    {
    	$route = route('vehicle-body-type.store');
    	$name = old('name');

    	$data = compact('route', 'name');

        return view('settings.vehicle-body-type.create-form', $data);
    }
    
    public function store(Request $request)
    {
    	$request->validate([
    		'name' => 'required|unique:vehicle_body_type,name'
    	]);

        $input = $request->only('name');

        VehicleBodyType::create($input);
        
        return redirect()->route('vehicle-body-type.create')->with('success', 'Vehicle Body Type Successfully added!');
    }
    
    public function edit($id)
    {
    	$route = route('vehicle-body-type.update', $id);
        $row = VehicleBodyType::whereId($id)->first();
    	$name = $row->name;

    	$data = compact('route', 'name');

        return view('settings.vehicle-body-type.create-form', $data);
    }
    
    public function update($id, Request $request)
    {
    	$request->validate([
    		'name' => 'required|unique:vehicle_body_type,name,' . $id . ',id'
    	]);

        $input = $request->only('name');

        VehicleBodyType::whereId($id)->update($input);

        return redirect()->route('vehicle-body-type.edit', $id)->with('success', 'Vehicle Body Type Successfully updated!');
    }
    
    public function show($id)
    {
        $row = VehicleBodyType::whereId($id)->first();
        $name = $row->name;

        $data = compact('name');

        return view('settings.vehicle-body-type.show', $data);
    }
}
