<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VehicleFuel;
use Validation;

class FuelController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        $query = new VehicleFuel;

        if($request->has('search'))
        {
            $search = $request->search;
            $query = $query->where('name', 'LIKE', "$search%");
        }

        $rows = $query->orderBy('name', 'ASC')->paginate(100);

        $data = compact('rows', 'search');

        return view('settings.fuel.index', $data);
    }
    
    public function create()
    {
    	$route = route('fuel.store');
    	$name = old('name');

    	$data = compact('route', 'name');

        return view('settings.fuel.create-form', $data);
    }
    
    public function store(Request $request)
    {
    	$request->validate([
    		'name' => 'required|unique:vehicle_fuel,name'
    	]);

        $input = $request->only('name');

        VehicleFuel::create($input);
        
        return redirect()->route('fuel.create')->with('success', 'Fuel Successfully added!');
    }
    
    public function edit($id)
    {
    	$route = route('fuel.update', $id);
        $row = VehicleFuel::whereId($id)->first();
    	$name = $row->name;

    	$data = compact('route', 'name');

        return view('settings.fuel.create-form', $data);
    }
    
    public function update($id, Request $request)
    {
    	$request->validate([
    		'name' => 'required|unique:vehicle_fuel,name,' . $id . ',id'
    	]);

        $input = $request->only('name');

        VehicleFuel::whereId($id)->update($input);

        return redirect()->route('fuel.edit', $id)->with('success', 'Fuel Successfully updated!');
    }
    
    public function show($id)
    {
        $row = VehicleFuel::whereId($id)->first();
        $name = $row->name;

        $data = compact('name');

        return view('settings.fuel.show', $data);
    }
}
