<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViolationType extends Model
{
     public $table = 'violation_type';
     public $timestamps = true;

     protected $fillable = [ 'name', 'price', 'created_at', 'updated_at' ];

    public function violation_list()
    {
        return $this->belongsTo('App\ViolationList', 'id', 'violation_type_id');
    }
}
