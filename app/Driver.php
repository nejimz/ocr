<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
     public $table = 'driver';
     public $timestamps = true;

     protected $fillable = ['license_number', 'license_type_id', 'expiry_date', 'agency_code', 'first_name', 'middle_name', 'last_name', 'birth_date', 'gender', 'nationality', 'height', 'weight', 'email', 'address', 'city', 'created_at', 'updated_at'];

     public function license()
     {
     	return $this->hasOne('App\DriverLicenseType', 'id', 'license_type_id');
     }

     public function vehicle()
     {
     	return $this->belongsTo('App\Vehicle', 'id', 'driver_id');
     }

     public function violation()
     {
          return $this->belongsTo('App\Violation', 'id', 'driver_id');
     }



     public function getCompleteLastNameAttribute()
     {
     	return $this->last_name . ', ' . $this->first_name . ' ' . $this->middle_name;
     }

     public function getCompleteFirstNameAttribute()
     {
          return $this->last_name . ', ' . $this->first_name . ' ' . $this->middle_name;
     }
}
