<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Violations extends Model
{
     public $table = 'violation';
     public $timestamps = true;

     protected $fillable = [ 'ticket_date', 'remarks', 'driver_id', 'vehicle_id', 'violation_type_id', 'user_id', 'created_at', 'updated_at' ];

    public function driver()
    {
        return $this->hasOne('App\Driver', 'id', 'driver_id');
    }

    public function vehicle()
    {
        return $this->hasOne('App\Vehicle', 'id', 'vehicle_id');
    }

    public function list()
    {
        return $this->belongsTo('App\ViolationList', 'id', 'violation_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}