<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViolationList extends Model
{
     public $table = 'violation_list';
     public $timestamps = true;

     protected $fillable = [ 'violation_id', 'violation_type_id', 'price', 'created_at', 'updated_at' ];

     public function violation()
     {
     	return $this->hasMany('App\Violations', 'id', 'violation_id');
     }

    public function type()
    {
        return $this->hasOne('App\ViolationType', 'id', 'violation_type_id');
    }
}
