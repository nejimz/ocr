<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleBodyType extends Model
{
     public $table = 'vehicle_body_type';
     public $timestamps = true;

     protected $fillable = ['name'];

     public function vehicle()
     {
     	return $this->belongsTo('App\Vehicle', 'id', 'license_type_id');
     }
}
