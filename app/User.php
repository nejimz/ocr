<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'access', 'active', 'created_at', 'updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAccessStatusAttribute()
    {
    	if($this->access == 1)
    	{
    		return 'ADMIN';
    	}
    	return 'NORMAL';
    }

    public function getActiveStatusAttribute()
    {
    	if($this->active == 1)
    	{
    		return 'YES';
    	}
    	return 'NO';
    }
}
