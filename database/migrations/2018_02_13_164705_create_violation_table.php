<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViolationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('violation', function (Blueprint $table) {
            $table->increments('id');
            $table->date('ticket_date');
            $table->text('remarks');
            $table->integer('driver_id')->unsigned();
            $table->integer('vehicle_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('driver_id')->references('id')->on('driver');
            $table->foreign('vehicle_id')->references('id')->on('vehicle');
            $table->foreign('user_id')->references('id')->on('users');

            #$table->index( [ 'id', 'driver_id', 'vehicle_id', 'violation_type_id', 'user_id' ] );
            $table->index( [ 'id', 'driver_id', 'vehicle_id', 'user_id' ] );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('violation');
    }
}
