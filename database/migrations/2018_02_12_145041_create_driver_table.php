<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver', function (Blueprint $table) {
            $table->increments('id');
            $table->string('license_number');
            $table->integer('license_type_id')->unsigned();
            $table->date('expiry_date');
            $table->string('agency_code');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->date('birth_date');
            $table->string('gender', 10);
            $table->string('nationality');
            $table->string('height');
            $table->string('weight');
            $table->string('email');
            $table->string('address');
            $table->string('city');
            $table->timestamps();
            
            $table->foreign('license_type_id')->references('id')->on('driver_license_type');
            $table->index( [ 'id', 'license_type_id' ] );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver');
    }
}
