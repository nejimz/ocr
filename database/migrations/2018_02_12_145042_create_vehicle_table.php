<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mv_file_number');
            $table->string('plate_number');
            $table->string('engine_number');
            $table->string('chasis_number');
            $table->string('denomination');
            $table->string('number_of_cylinders');
            $table->integer('fuel_id')->unsigned();
            $table->string('make');
            $table->string('series');
            $table->integer('body_type_id')->unsigned();
            $table->string('body_number');
            $table->string('year_model');
            $table->string('license_number');
            $table->timestamps();

            $table->foreign('fuel_id')->references('id')->on('vehicle_fuel');
            $table->foreign('body_type_id')->references('id')->on('vehicle_body_type');
            
            $table->index( [ 'id', 'fuel_id', 'body_type_id' ] );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle');
    }
}
