<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViolationListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('violation_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('violation_id')->unsigned();
            $table->integer('violation_type_id')->unsigned();
            $table->double('price', 10, 2);
            $table->timestamps();

            $table->foreign('violation_id')->references('id')->on('violation');
            $table->foreign('violation_type_id')->references('id')->on('violation_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('violation_list');
    }
}
