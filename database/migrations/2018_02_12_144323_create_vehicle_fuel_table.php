<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\VehicleFuel;

class CreateVehicleFuelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_fuel', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            
            $table->index( [ 'id' ] );
        });

        VehicleFuel::create(['name' => 'GAS']);
        VehicleFuel::create(['name' => 'DIESEL']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_fuel');
    }
}
