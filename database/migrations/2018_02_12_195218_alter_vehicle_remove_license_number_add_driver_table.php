<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVehicleRemoveLicenseNumberAddDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle', function (Blueprint $table) {
            // remove column
            if (Schema::hasColumn('vehicle', 'license_number'))
            {
                $table->dropColumn('license_number');
            }
            // add column
            $table->integer('driver_id')->unsigned();
            $table->foreign('driver_id')->references('id')->on('driver');
            
            $table->index('driver_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
