@extends('layouts.master')

@section('content')
    <br>
    <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="#">Violations</a></li>
                    <li class="is-active"><a href="#" aria-current="page">Upload Image</a></li>
                </ul>
            </nav>
        </header>
        <div class="card-content">
            @include('layouts.validation-messages')
            <div class="content">
    
                <form action="{{ $route }}" method="post" enctype="multipart/form-data">
                    <div class="columns">
                        <div class="column is-9">

                            <div class="field is-horizontal">
                                <div class="field-label is-normal"></div>
                                <div class="field-body">
                                    <div class="file has-name is-centered is-info">
                                        <label class="file-label">
                                            <input class="file-input" type="file" name="photo" accept="image/*">
                                            <span class="file-cta">
                                                <span class="file-icon">
                                                    <i class="fa fa-upload"></i>
                                                </span>
                                                <span class="file-label">Choose a file…</span>
                                            </span>
                                        <span class="file-name">Screen Shot {{ date('Y-m-d @ h:m:i') }}.png</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label"></div>
                                <div class="field-body">
                                    <div class="field">
                                        <div class="control">
                                            <button class="button is-info is-normal">Submit</button>
                                            {!! csrf_field() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection