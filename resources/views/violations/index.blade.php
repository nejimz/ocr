@extends('layouts.master')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-exclamation-triangle"></i> Violations</h3>
        </div>
        <div class="column is-3">
            <form action="" method="get">
                <div class="field has-addons">
                    <div class="control">
                        <a class="button is-static"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="control">
                        <input class="input is-normal" type="text" name="search" id="search" value="{{ $search }}">
                    </div>
                    <div class="control">
                        <button class="button is-dark">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr>
                <td width="5%"></td>
                <td width="25%">Driver</td>
                <td width="10%">Plate Number</td>
                <td width="20%">Violation</td>
                <td width="10%">Amount</td>
                <td width="10%">Date</td>
                <td width="20%">Officer</td>
            </tr>
        </thead>
        <tbody>
            @foreach($rows as $violation)
            <?php $penalty_amout = 0; ?>
            <tr>
                <td class="has-text-centered">
                    @if(Auth::user()->access == 1)
                    <a href="{{ route('violations.edit', $violation->id) }}" title="Edit violations details">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ route('violations.show', $violation->id) }}" title="Show violations details">
                        <i class="fa fa-list-alt"></i>
                    </a>
                    @else
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-list-alt"></i>
                    @endif
                </td>
                <td>
                    <a href="{{ route('drivers.show', $violation->driver_id) }}" title="Click to show driver details">
                        {{ $violation->driver->complete_last_name }}
                    </a>
                </td>
                <td>
                    <a href="{{ route('vehicles.show', $violation->vehicle_id) }}" title="Click to show vehicle details">
                        {{ $violation->vehicle->plate_number }}
                    </a>
                </td>
                <td>
                    @foreach($violation->list->whereViolationId($violation->id)->get() as $row)
                        <u>{{ $row->type->name }}</u>
                        <?php $penalty_amout += $row->type->price; ?>
                    @endforeach
                </td>
                <td>{{ number_format($penalty_amout, 2) }}</td>
                <td>{{ $violation->ticket_date }}</td>
                <td>{{ $violation->user->name }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $rows->appends(['search'=>$search])->links('vendor.pagination.default') !!}
        
@endsection
