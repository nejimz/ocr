@extends('layouts.master')

@section('content')
    <br>
    <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="{{ route('violations.index') }}">Violations</a></li>
                    <li class="is-active"><a href="#" aria-current="page">Details</a></li>
                </ul>
            </nav>
        </header>
        <div class="card-content">
            <div class="columns">
                <div class="column is-8">
                    <div class="field is-horizontal">
                        <div class="field-label">
                            <label class="label" for="driver_name">Driver</label>
                        </div>
                        <div class="field-body">
                            <div class="field is-expanded">
                                <p class="control">
                                    {{ $driver_name }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label">
                            <label class="label" for="driver_name">License No.</label>
                        </div>
                        <div class="field-body">
                            <div class="field is-expanded">
                                <p class="control">
                                    {{ $license_number }}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="field is-horizontal">
                        <div class="field-label">
                            <label class="label" for="ticket_date">Vehicle</label>
                        </div>
                        <div class="field-body">
                            <div class="field is-narrow">
                                <p class="control">
                                    {{ $plate_number }}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="field is-horizontal">
                        <div class="field-label">
                            <label class="label" for="ticket_date">Ticket Date</label>
                        </div>
                        <div class="field-body">
                            <div class="field is-narrow">
                                <p class="control">
                                    {{ date('F d, Y', strtotime($ticket_date)) }}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="field is-horizontal">
                        <div class="field-label">
                            <label class="label" for="remarks">Remarks</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <p class="control is-expanded">
                                    {{ $remarks }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="column is-4">
                    <table id="violations" class="table is-bordered is-narrow is-hoverable is-fullwidth">
                        <thead>
                            <tr>
                                <th width="100%">Violation</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($violation_list)
                            @foreach($violation_list as $value)
                                <?php
                                $row = explode("`", $value);
                                ?>
                                <tr class="violation-row-{{ $row[0] }}">
                                    <td>
                                        {{ $row[1] }}
                                        <input type="hidden" class="violations_list" name="violation_list[{{ $row[0] }}]" value="{{ $value }}">
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
