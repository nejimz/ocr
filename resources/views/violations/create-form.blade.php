
@extends('layouts.master')

@section('content')
    <br>
    <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="#">Violations</a></li>
                    <li class="is-active"><a href="#" aria-current="page">Form</a></li>
                </ul>
            </nav>
        </header>
        <div class="card-content">
            @if($plate_number != '')
                @include('layouts.validation-messages')
            @endif
            <div class="content">
    
                <form action="{{ $route }}" method="post">
                    <div class="columns">
                        <div class="column is-6">


                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="driver_name">Driver</label>
                                </div>
                                <div class="field-body">
                                    <div class="field is-expanded">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="driver_name" id="driver_name" value="{{ $driver_name }}">
                                        </p>
                                    </div>
                                    <div class="field is-expanded">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="license_number" id="license_number" value="{{ $license_number }}" readonly="">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="ticket_date">Vehicle</label>
                                </div>
                                <!--div class="field-body">
                                    <div class="field is-narrow">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="plate_number" id="plate_number" value="{{ $plate_number }}" readonly="">
                                        </p>
                                    </div>
                                </div-->
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded">
                                            <textarea class="textarea" name="plate_number" id="plate_number" cols="20" rows="5">{{ $plate_number }}</textarea>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="ticket_date">Ticket Date</label>
                                </div>
                                <div class="field-body">
                                    <div class="field is-narrow">
                                        <p class="control">
                                            <input class="input is-normal" type="date" name="ticket_date" id="ticket_date" value="{{ $ticket_date }}">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="remarks">Remarks</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded">
                                            <textarea class="textarea" name="remarks" id="remarks" cols="20" rows="5">{{ $remarks }}</textarea>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label"></div>
                                <div class="field-body">
                                    <div class="field">
                                        <div class="control">
                                            <button class="button is-info is-normal">Submit</button>
                                            {!! csrf_field() !!}
                                            {{ ((Request::segment(4) == 'edit')? method_field('PUT') : '') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="column is-6">
                            <div class="field is-horizontal">
                                <div class="field-body">
                                    <div class="field is-expanded">
                                        <div class="control">
                                            <div class="select is-fullwidth">
                                                <select name="violation" id="violation" onchange="select(this)">
                                                    <option value=""></option>
                                                @foreach($violations as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name . ' - ' . number_format($row->price, 2) }}</option>
                                                @endforeach
                                                </select>
                                                <input type="hidden" id="violation_count" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <table id="violations" class="table is-bordered is-narrow is-hoverable is-fullwidth">
                                <thead>
                                    <tr>
                                        <th width="10%" class="has-text-centered"></th>
                                        <th width="90%">Violation</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if($violation_list)
                                    @foreach($violation_list as $value)
                                        <?php
                                        $row = explode("`", $value);
                                        ?>
                                        <tr class="violation-row-{{ $row[0] }}">
                                            <td class="has-text-centered">
                                                <a onclick="remove_violation('.violation-row-{{ $row[0] }}')" href="javascript:void(0)"><i class="fa fa-times"></i></a>
                                            </td>
                                            <td>
                                                {{ $row[1] }}
                                                <input type="hidden" class="violations_list" name="violation_list[{{ $row[0] }}]" value="{{ $value }}">
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <div class="modal @if($plate_number == '') is-active @endif">
        <div class="modal-background" onclick="return false"></div>
        <div class="modal-content">
            <div class="box">
                <!--form action="" method="post" enctype="multipart/form-data">
                    <div class="file has-name is-centered is-info">
                        <label class="file-label">
                            <input class="file-input" type="file" name="resume" accept="image/*">
                            <span class="file-cta">
                                <span class="file-icon">
                                    <i class="fa fa-upload"></i>
                                </span>
                                <span class="file-label">Choose a file…</span>
                            </span>
                        <span class="file-name">Screen Shot 2017-07-29 at 15.54.25.png</span>
                        </label>
                    </div>
                </form-->

                @if($plate_number == '')
                    @include('layouts.validation-messages')
                @endif
    
                <form action="{{ $route_upload }}" method="post" enctype="multipart/form-data">
                    <div class="columns">
                        <div class="column is-10">

                            <div class="field is-horizontal">
                                <div class="field-label is-normal"></div>
                                <div class="field-body">
                                    <div class="file has-name is-centered is-info">
                                        <label class="file-label">
                                            <input class="file-input" type="file" name="photo" accept="image/*">
                                            <span class="file-cta">
                                                <span class="file-icon">
                                                    <i class="fa fa-upload"></i>
                                                </span>
                                                <span class="file-label">Choose a file…</span>
                                            </span>
                                        <span class="file-name">Screen Shot {{ date('Y-m-d @ h:m:i') }}.png</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label"></div>
                                <div class="field-body">
                                    <div class="field">
                                        <div class="control">
                                            <button class="button is-info is-normal">Submit</button>
                                            <a class="button is-normal" href="{{ route('violations.index') }}">Violations</a>
                                            {!! csrf_field() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection

@push('scripts')
<input type="hidden" id="json_search" value="{{ route('json_search') }}">

<script type="text/javascript">
    var xhr_driver_name = null;
    $(function(){
        $("#driver_name").autocomplete({
            source: function (request, response) 
            {
                xhr_driver_name = $.ajax({
                    type : 'get',
                    url : $("#json_search").val(),
                    data : 'search=' + request.term,
                    cache : true,
                    dataType : "json",
                    beforeSend: function(xhr) 
                    {
                        if (xhr_driver_name != null)
                        {
                            xhr_driver_name.abort();
                        }
                    }
                }).done(function(data) 
                {
                    response($.map( data, function(value, key) 
                    {
                        return { label: value, value: key }
                    }));

                }).fail(function(jqXHR, textStatus) 
                {
                    //console.log('Request failed: ' + textStatus);
                });
            }, 
            minLength: 2,
            autoFocus: true,
            select: function(event, ui)
            {
                var name = ui.item.label;
                var license_number = ui.item.value;

                $("#driver_name").val(name);
                $("#license_number").val(license_number);

                return false;
            }
        });
    });

    function select(key) {
    //$(function(){
        //$('#violation').change(function(){
            var add_bool = true;
            var violation_count = parseInt($('#violation_count').val());
            //var key = $(this).val();
            var key = key.value;
            var value = $('#violation option:selected').text();
            var violations_length = $('.violations_list').length;

            var row = '<tr class="violation-row-' + key + '">' + 
                '<td class="has-text-centered"><a onclick="remove_violation(\'.violation-row-' + key + '\')" href="javascript:void(0)"><i class="fa fa-times"></i></a></td>' + 
                '<td>' + value + '<input type="hidden" class="violations_list" name="violation_list[' + key + ']" value="' + key + '`' + value + '"></td></tr>';

            if(violations_length > 0)
            {
                $('.violations_list').each(function(){
                    var arr = $(this).val().split("`");
                    console.log(arr[0] + '=' + key);
                    if(arr[0] == key)
                    {
                        add_bool = false;
                        return false;
                    }
                });
            }

            if(add_bool)
            {
                violation_count++;
                $('#violation_count').val(violation_count);
                $('#violations').append(row);
            }

            $('#violation').val('');

        //});
    //});
    }

    function remove_violation(row)
    {
        $(row).remove();
        return false;
    }
</script>
@endpush