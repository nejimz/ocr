@extends('layouts.master')

@section('content')
    <br>
    <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="{{ route('drivers.index') }}">Drivers</a></li>
                    <li class="is-active"><a href="#" aria-current="page">Form</a></li>
                </ul>
            </nav>
            </header>
            <div class="card-content">
                @include('layouts.validation-messages')
                <div class="content">
        
                    <form action="{{ $route }}" method="post">

                        <div class="columns">
                            <div class="column is-8">

                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label is-normal" for="license_number">License No.</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field is-expanded">
                                            <p class="control">
                                                <input class="input is-normal" type="text" name="license_number" id="license_number" value="{{ $license_number }}">
                                            </p>
                                        </div>
                                        <div class="field">
                                            <div class="control is-expanded">
                                                <div class="select is-fullwidth">
                                                    <select name="license_type_id" id="license_type_id">
                                                        <option value="">SELECT LICENSE TYPE</option>
                                                        @foreach($license_types as $row)
                                                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="field is-expanded">
                                            <p class="control">
                                                <input class="input is-normal" type="date" name="expiry_date" id="expiry_date" value="{{ $expiry_date }}" placeholder="Expiry Date">
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label is-normal" for="agency_code">Agency Code</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field is-narrow">
                                            <p class="control">
                                                <input class="input is-normal" type="text" name="agency_code" id="agency_code" value="{{ $agency_code }}">
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label is-normal" for="name">Name</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <p class="control is-expanded">
                                                <input class="input is-normal" type="text" name="first_name" id="first_name" placeholder="First Name" value="{{ $first_name }}">
                                            </p>
                                        </div>
                                        <div class="field">
                                            <p class="control is-expanded">
                                                <input class="input is-normal" type="text" name="middle_name" id="middle_name" placeholder="Middle Name" value="{{ $middle_name }}">
                                            </p>
                                        </div>
                                        <div class="field">
                                            <p class="control is-expanded">
                                                <input class="input is-normal" type="text" name="last_name" id="last_name" placeholder="Last Name" value="{{ $last_name }}">
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label is-normal" for="birth_date">Birth Date</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field is-narrow">
                                            <p class="control">
                                                <input class="input is-normal" type="date" name="birth_date" id="birth_date" value="{{ $birth_date }}">
                                            </p>
                                        </div>
                                        <div class="field is-narrow">
                                            <div class="control">
                                                <div class="select is-fullwidth">
                                                    <select name="gender" id="gender">
                                                        <option value="">SELECT GENDER</option>
                                                        <option value="male">MALE</option>
                                                        <option value="female">FEMALE</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label is-normal" for="nationality">Nationality</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field is-expanded">
                                            <p class="control">
                                                <input class="input is-normal" type="text" name="nationality" id="nationality" value="{{ $nationality }}">
                                            </p>
                                        </div>
                                        <div class="field is-expanded">
                                            <p class="control has-icons-right">
                                                <input class="input is-normal" type="text" name="height" id="height" value="{{ $height }}" placeholder="Height (cm)">
                                                <span class="icon is-small is-right">
                                                    cm
                                                </span>
                                            </p>
                                        </div>
                                        <div class="field is-expanded">
                                            <p class="control has-icons-right">
                                                <input class="input is-normal" type="text" name="weight" id="weight" value="{{ $weight }}" placeholder="Weight (kg)">
                                                <span class="icon is-small is-right">
                                                    kg
                                                </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label is-normal" for="email">E-Mail</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <p class="control">
                                                <input class="input is-normal" type="email" name="email" id="email" value="{{ $email }}">
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label is-normal" for="address">Address</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <p class="control is-expanded">
                                                <input class="input is-normal" type="text" name="address" id="address" value="{{ $address }}">
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label is-normal">
                                        <label class="label is-normal" for="city">City</label>
                                    </div>
                                    <div class="field-body">
                                        <div class="field">
                                            <p class="control is-expanded">
                                                <input class="input is-normal" type="text" name="city" id="city" value="{{ $city }}">
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="field is-horizontal">
                                    <div class="field-label"></div>
                                    <div class="field-body">
                                        <div class="field">
                                            <div class="control">
                                                <button class="button is-info is-normal">Submit</button>
                                                {!! csrf_field() !!}
                                                {{ ((Request::segment(4) == 'edit')? method_field('PUT') : '') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection

@push('scripts')
<script type="text/javascript">
    $('#license_type_id').val('{{ $license_type_id }}');
    $('#gender').val('{{ $gender }}');
    //$('#license_type_id option[value={{ $license_type_id or 0 }}]').attr('selected','selected');
    //$('#gender option[value={{ $gender or 0 }}]').attr('selected','selected');
</script>
@endpush