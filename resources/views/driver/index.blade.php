@extends('layouts.master')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-users"></i>Drivers</h3>
        </div>
        <div class="column is-3">
            <form action="" method="get">
                <div class="field has-addons">
                    <div class="control">
                        <a class="button is-static"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="control">
                        <input class="input is-normal" type="text" name="search" id="search" value="{{ $search }}">
                    </div>
                    <div class="control">
                        <button class="button is-dark">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr>
                <th width="6%"></th>
                <th width="15%">License Number</th>
                <th width="15%">Expiry Date</th>
                <th width="16%">License Type</th>
                <th width="15%" class="has-text-centered">Agency Code</th>
                <th width="25%">Name</th>
                <th width="8%" class="has-text-centered">Gender</th>
            </tr>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td class="has-text-centered">
                    @if(Auth::user()->access == 1)
                    <a href="{{ route('drivers.edit', $row->id) }}" title="Edit Driver Details">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ route('drivers.show', $row->id) }}" title="Show Driver Details">
                        <i class="fa fa-list-alt"></i>
                    </a>
                    @else
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-list-alt"></i>
                    @endif
                </td>
                <td>{{ $row->license_number }}</td>
                <td>{{ $row->expiry_date }}</td>
                <td>{{ $row->license->name }}</td>
                <td class="has-text-centered">{{ $row->agency_code }}</td>
                <td>{{ $row->last_name . ', ' . $row->first_name . ' ' . $row->middle_name }}</td>
                <td class="has-text-centered is-uppercase">{{ $row->gender }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $rows->appends(['search'=>$search])->links('vendor.pagination.default') !!}
        
@endsection
