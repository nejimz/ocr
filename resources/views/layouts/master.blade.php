<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>OCR - Optical Character Recognition</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/jquery.css') }}" rel="stylesheet">
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
    </head>
    <body>
        <div id="app">
            <nav class="navbar has-shadow is-dark" role="navigation" aria-label="main navigation">
                <div class="container">
                    <div class="navbar-brand">
                        <a href="{{ route('home') }}" class="navbar-item">OCR</a>
                        <div class="navbar-burger burger" data-target="navMenu" onclick="toggleBurger()">
                            <span></span><span></span><span></span>
                        </div>
                    </div>
                    <?php
                    $segment = Request::segment(2);
                    $is_active = ' is-active';

                    $home = ($segment == '')? $is_active: '';
                    $violations = ($segment == 'violations')? $is_active: '';
                    $drivers = ($segment == 'drivers')? $is_active: '';
                    $vehicles = ($segment == 'vehicles')? $is_active: '';
                    $reports = ($segment == 'reports')? $is_active: '';
                    $settings = ($segment == 'settings')? $is_active: '';
                    ?>
                    <div class="navbar-menu" id="navMenu">
                        <div class="navbar-start">
                            <a class="navbar-item" href="{{ route('home') }}">
                                <i class="fa fa-home "></i> Home
                            </a>

                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link {{ $violations }}" href="{{ route('violations.index') }}" title="Violations"><i class="fa fa-exclamation-triangle"></i>&nbsp;Violations</a>
                                @if(Auth::user()->access == 1)
                                <div class="navbar-dropdown is-boxed">
                                    <a class="navbar-item" href="{{ route('violations.create') }}" title="Create"><i class="fa fa-plus"></i>&nbsp;Create</a>
                                </div>
                                @endif
                            </div>

                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link {{ $drivers }}" href="{{ route('drivers.index') }}" title="Drivers"><i class="fa fa-users"></i>&nbsp;Drivers</a>
                                @if(Auth::user()->access == 1)
                                <div class="navbar-dropdown is-boxed">
                                    <a class="navbar-item" href="{{ route('drivers.create') }}" title="Register Driver"><i class="fa fa-plus"></i>&nbsp;Add</a>
                                </div>
                                @endif
                            </div>

                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link {{ $vehicles }}" href="{{ route('vehicles.index') }}" title="Vehicles"><i class="fa fa-car"></i>&nbsp;Vehicles</a>
                                @if(Auth::user()->access == 1)
                                <div class="navbar-dropdown is-boxed">
                                    <a class="navbar-item" href="{{ route('vehicles.create') }}" title="Register Vehicles"><i class="fa fa-plus"></i>&nbsp;Add</a>
                                </div>
                                @endif
                            </div>

                            @if(Auth::user()->access == 1)
                            <!-- is-hidden-desktop is-hidden-touch-->
                            <a class="navbar-item" href="{{ route('report-violation.create') }}" title="Reports">
                                <i class="fa fa-copy"></i>&nbsp;Reports
                            </a>

                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link {{ $settings }}" href="javascript:void(0)" title="Users"><i class="fa fa-cogs"></i>&nbsp;Settings</a>
                                <div class="navbar-dropdown is-boxed">
                                    <a class="navbar-item" href="{{ route('users.create') }}" title="Create"><i class="fa fa-users"></i>&nbsp;User</a>
                                    <a class="navbar-item" href="{{ route('violation-type.create') }}" title="Create"><i class="fa fa-exclamation-triangle"></i>&nbsp;Violation Type</a>
                                    <a class="navbar-item" href="{{ route('vehicle-body-type.create') }}" title="Create"><i class="fa fa-car"></i>&nbsp;Vehicle Body Type</a>
                                    <a class="navbar-item" href="{{ route('fuel.create') }}" title="Create"><i class="fa fa-tint"></i>&nbsp;Fuel</a>
                                </div>
                            </div>
                            @endif

                        </div>

                        <div class="navbar-end">
                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link" href="#"><i class="fa fa-user"></i>&nbsp;{{ Auth::user()->name }}</a>

                                <div class="navbar-dropdown">
                                    <a href="{{ route('reset_password_form') }}" class="navbar-item" title="Reset Password">Reset Password</a>
                                    <a href="javascript::void(0)" class="navbar-item" onclick="document.getElementById('logout-form').submit();">Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </nav>
            <div class="container">@yield('content')</div>
        </div>
        <active-test></active-test>
        <footer class="footer">
            <div class="container">
                <div class="content has-text-centered">
                    <p>
                        &copy; OCR, Inc {{ date('Y') }}
                    </p>
                </div>
            </div>
        </footer>
        @stack('scripts')
        <script type="text/javascript">
            
        /*$('#navbar-burger').click(function() {
            $(this).toggleClass('is-active');
            $menu.toggleClass('is-active');
        });*/
        
        function toggleBurger()
        {
            var burger = $('.burger');
            var menu = $('.navbar-menu');
            burger.toggleClass('is-active');
            menu.toggleClass('is-active');
        }
        </script>
        <!--script type="text/javascript"-->
        <!--
        // START  Laravel Call Scripts every page
        // END    Laravel Call Scripts every page 
        /*date = new bulmaCalendar(document.querySelector('.sr-date'), {
          dataFormat: 'd MM yyyy' // 1 January 2018
        });*/
        /*document.addEventListener( 'DOMContentLoaded', function () {
            var datePicker = new bulmaCalendar( document.getElementById( 'datepicker1' ), {
                startDate: new Date(), // Date selected by default
                dateFormat: 'yyyy-mm-dd', // the date format `field` value
                lang: 'en', // internationalization
                overlay: false,
                closeOnOverlayClick: true,
                closeOnSelect: true,
                // callback functions
                onSelect: null,
                onOpen: null,
                onClose: null,
                onRender: null
            } );
            var datePicker = new bulmaCalendar( document.getElementById( 'datepicker2' ), {
                overlay: true
            } );
        } );*/-->
        <!--/script-->
    </body>
</html>
