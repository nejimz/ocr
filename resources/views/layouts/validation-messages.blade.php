@if(Session::has('success'))
    <article class="message is-success">
        <div class="message-header">
            <p>Success Message</p>
        </div>
        <div class="message-body">{{ Session::get('success') }}</div>
    </article>
@elseif(count($errors) > 0)
    <article class="message is-warning">
        <div class="message-header">
            <p>Warning Message</p>
        </div>
        <div class="message-body">
            <ul class="no-bullet">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            
        </div>
    </article>
@elseif(Session::has('warning'))
    <article class="message is-warning">
        <div class="message-header">
            <p>Warning Message</p>
        </div>
        <div class="message-body">{{ Session::get('warning') }}</div>
    </article>
@endif