@extends('layouts.master')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-exclamation-triangle"></i> Violation Types</h3>
        </div>
        <div class="column is-3">
            <form action="" method="get">
                <div class="field has-addons">
                    <div class="control">
                        <a class="button is-static"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="control">
                        <input class="input is-normal" type="text" name="search" id="search" value="{{ $search }}">
                    </div>
                    <div class="control">
                        <button class="button is-dark">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr>
                <td width="5%"></td>
                <td width="95%">Name</td>
            </tr>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td class="has-text-centered">
                    <a href="{{ route('fuel.edit', $row->id) }}" title="Edit Fuel Details">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>
                <td>{{ $row->name }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {!! $rows->appends(['search'=>$search])->links('vendor.pagination.default') !!}
        
@endsection
