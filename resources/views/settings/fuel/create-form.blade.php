@extends('layouts.master')

@section('content')
    <br>
    <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="{{ route('fuel.index') }}">Fuel</a></li>
                    <li class="is-active"><a href="#" aria-current="page">Form</a></li>
                </ul>
            </nav>
        </header>
        <div class="card-content">
            @include('layouts.validation-messages')
            <div class="content">
    
                <form action="{{ $route }}" method="post">
                    <div class="columns">
                        <div class="column is-9">

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="mv_file_number">Name</label>
                                </div>
                                <div class="field-body">
                                    <div class="field is-expanded">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="name" id="mv_file_number" value="{{ $name }}">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label"></div>
                                <div class="field-body">
                                    <div class="field">
                                        <div class="control">
                                            <button class="button is-info is-normal">Submit</button>
                                            {!! csrf_field() !!}
                                            {{ ((Request::segment(5) == 'edit')? method_field('PUT') : '') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection