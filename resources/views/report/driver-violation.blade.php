@extends('layouts.master')

@section('content')
    <br>
    <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="#">Reports</a></li>
                    <li class="is-active"><a href="#" aria-current="page">Violation</a></li>
                </ul>
            </nav>
        </header>
        <div class="card-content">
            @include('layouts.validation-messages')
            <div class="content">
    
                <form action="{{ $route }}" method="get" target="_blank">
                    <div class="columns">
                        <div class="column is-6">

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="ticket_date">Ticket Date</label>
                                </div>
                                <div class="field-body">
                                    <div class="field is-narrow">
                                        <p class="control">
                                            <input class="input is-normal" type="date" name="ticket_date" id="ticket_date" value="{{ $ticket_date }}">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label"></div>
                                <div class="field-body">
                                    <div class="field">
                                        <div class="control">
                                            <button class="button is-info is-normal">Submit</button>
                                            {!! csrf_field() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

<script type="text/javascript">
</script>
@endpush