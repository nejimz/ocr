@extends('layouts.plain')

@section('content')

<h4>Ticket Date - {{ $ticket_date->toDateString() }}</h4>

<table class="table is-bordered is-narrow is-hoverable is-fullwidth">
    <thead>
        <tr>
            <th>Driver</th>
            <th>Vehicle</th>
            <th>Violation</th>
            <th>Ticketing Officer</th>
            <th>Remarks</th>
        </tr>
    </thead>
    <tbody>
        @foreach($rows as $row)
            <tr>
                <td>{{ $row->driver->complete_last_name }}</td>
                <td>{{ $row->vehicle->plate_number }}</td>
                <td>
                    @foreach($row->list->whereViolationId($row->id)->get() as $violation)
                        <u>{{ $violation->type->name }}</u>
                    @endforeach
                </td>
                <td>{{ $row->user->name }}</td>
                <td>{{ $row->remarks }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

@endsection