@extends('layouts.app')

@section('content')

    <section class="hero is-dark">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                    OCR (Optical Character Recognition)
                </h1>
            </div>
        </div>
    </section>

    <div class="columns is-marginless is-centered">
        <div class="column is-4">
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">Please Login to proceed.</p>
                </header>

                <div class="card-content">
                    <form class="login-form" method="POST" action="{{ route('login_auth') }}">
                        {{ csrf_field() }}

                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <input class="input" id="email" type="email" name="email" placeholder="E-Mail" value="{{ old('email') }}" required autofocus>
                                    </p>

                                    @if ($errors->has('email'))
                                        <p class="help is-danger">
                                            {{ $errors->first('email') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <input class="input" id="password" type="password" name="password" placeholder="Password" required>
                                    </p>
                                    @if ($errors->has('password'))
                                        <p class="help is-danger">
                                            {{ $errors->first('password') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="field">
                            <button type="submit" class="button is-fullwidth is-dark">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
