@extends('layouts.master')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-users"></i> Users</h3>
        </div>
        <div class="column is-3">
            <form action="" method="get">
                <div class="field has-addons">
                    <div class="control">
                        <a class="button is-static"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="control">
                        <input class="input is-normal" type="text" name="search" id="search" value="{{ $search }}">
                    </div>
                    <div class="control">
                        <button class="button is-dark">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr>
                <td width="5%"></td>
                <td width="25%">Name</td>
                <td width="20%">Email</td>
                <td width="10%">Access</td>
                <td width="10%">Active</td>
                <td width="15%">Created At</td>
                <td width="15%">Updated At</td>
            </tr>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td class="has-text-centered">
                    @if(Auth::user()->access == 1)
                    <a href="{{ route('users.edit', $row->id) }}" title="Edit User Details">
                        <i class="fa fa-edit"></i>
                    </a>
                    @else
                        <i class="fa fa-edit"></i>
                    @endif
                    <a href="{{ route('users.show', $row->id) }}" title="Show User Details">
                        <i class="fa fa-list-alt"></i>
                    </a>
                </td>
                <td>{{ $row->name }}</td>
                <td>{{ $row->email }}</td>
                <td>{{ $row->access_status }}</td>
                <td>{{ $row->active_status }}</td>
                <td>{{ $row->created_at }}</td>
                <td>{{ $row->updated_at }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $rows->appends(['search'=>$search])->links('vendor.pagination.default') !!}
        
@endsection
