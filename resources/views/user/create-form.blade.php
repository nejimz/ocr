@extends('layouts.master')

@section('content')
    <br>
    <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="{{ route('users.index') }}">User</a></li>
                    <li class="is-active"><a href="#" aria-current="page">Form</a></li>
                </ul>
            </nav>
        </header>
        <div class="card-content">
            @include('layouts.validation-messages')
            <div class="content">
                <form action="{{ $route }}" method="post">

                    <div class="columns">
                        <div class="column is-7">

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="name">Name</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded">
                                            <input class="input is-normal" type="text" name="name" id="name" value="{{ $name }}">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="email">E-Mail</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded">
                                            <input class="input is-normal" type="email" name="email" id="email" value="{{ $email }}">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="access">Access</label>
                                </div>
                                <div class="field-body">
                                    <div class="field is-narrow">
                                        <div class="control">
                                            <div class="select is-fullwidth">
                                                <select name="access" id="access">
                                                    <option value="1">Admin</option>
                                                    <option value="2">Normal</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="access">Active</label>
                                </div>
                                <div class="field-body">
                                    <div class="field is-narrow">
                                        <div class="control">
                                            <div class="select is-fullwidth">
                                                <select id="active" name="active">
                                                    <option value="0">No</option>
                                                    <option value="1">Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if(Request::segment(5) == 'edit')
                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="name">Password</label>
                                </div>
                                <div class="field-body">
                                    <div class="field has-addons">
                                        <div class="control">
                                        </div>
                                        <div class="control">
                                            <input class="input is-normal" type="text" name="password" id="password" value="" readonly="">
                                        </div>
                                        <div class="control">
                                            <a href="javascript:void(0)" onclick="generate_password({{ Request::segment(4) }})" class="button is-dark"><i class="fa fa-refresh"></i>&nbsp;&nbsp;Generate</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{ method_field('PUT') }}
                            @endif

                            <div class="field is-horizontal">
                                <div class="field-label"></div>
                                <div class="field-body">
                                    <div class="field">
                                        <div class="control">
                                            <button class="button is-info is-normal">Submit</button>
                                            {!! csrf_field() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<input type="hidden" id="generate_password" value="{{ route('generate_password') }}">
<script type="text/javascript">
    var xhr_generate_password = null;

    $('#access option[value={{ $access or 0 }}]').attr('selected','selected');
    $('#active option[value={{ $active or 0 }}]').attr('selected','selected');

    function generate_password(id)
    {
        xhr_generate_password = $.ajax({
            type : 'get',
            url : $("#generate_password").val(),
            data : 'id=' + id,
            dataType : "json",
            beforeSend: function(xhr) 
            {
                if (xhr_generate_password != null)
                {
                    xhr_generate_password.abort();
                }
            }
        }).done(function(data) 
        {
            $('#password').val(data['password']);
        }).fail(function(jqXHR, textStatus) 
        {
            //console.log('Request failed: ' + textStatus);
        });
    }
</script>
@endpush