@extends('layouts.master')

@section('content')
    <br>
    <div class="columns">
        <div class="column is-9">
            <h3 class="title is-3"><i class="fa fa-car"></i> Vehicles</h3>
        </div>
        <div class="column is-3">
            <form action="" method="get">
                <div class="field has-addons">
                    <div class="control">
                        <a class="button is-static"><i class="fa fa-search"></i></a>
                    </div>
                    <div class="control">
                        <input class="input is-normal" type="text" name="search" id="search" value="{{ $search }}">
                    </div>
                    <div class="control">
                        <button class="button is-dark">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="table is-bordered is-narrow is-hoverable is-fullwidth">
        <thead>
            <tr>
                <td width="5%"></td>
                <td width="15%">MV File No.</td>
                <td width="15%">Plate Number</td>
                <td width="10%">Body Type</td>
                <td width="10%">Fuel</td>
                <td width="15%">Make</td>
                <td width="30%">Owner</td>
            </tr>
        </thead>
        <tbody>
            @foreach($rows as $row)
            <tr>
                <td class="has-text-centered">
                    @if(Auth::user()->access == 1)
                    <a href="{{ route('vehicles.edit', $row->id) }}" title="Edit Vehicle Details">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ route('vehicles.show', $row->id) }}" title="Show Vehicle Details">
                        <i class="fa fa-list-alt"></i>
                    </a>
                    @else
                    <i class="fa fa-edit"></i>
                    <i class="fa fa-list-alt"></i>
                    @endif
                </td>
                <td>{{ $row->mv_file_number }}</td>
                <td>{{ $row->plate_number }}</td>
                <td>{{ $row->body_type->name }}</td>
                <td>{{ $row->fuel->name }}</td>
                <td>{{ $row->make }}</td>
                <td>
                    {{ $row->driver->complete_last_name }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {!! $rows->appends(['search'=>$search])->links('vendor.pagination.default') !!}
        
@endsection
