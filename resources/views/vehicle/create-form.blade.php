@extends('layouts.master')

@section('content')
    <br>
    <div class="card">
        <header class="card-header">
            &nbsp;&nbsp;&nbsp;&nbsp;
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="{{ route('vehicles.index') }}">Vehicles</a></li>
                    <li class="is-active"><a href="#" aria-current="page">Form</a></li>
                </ul>
            </nav>
        </header>
        <div class="card-content">
            @include('layouts.validation-messages')
            <div class="content">
    
                <form action="{{ $route }}" method="post">
                    <div class="columns">
                        <div class="column is-9">

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="mv_file_number">MV File No.</label>
                                </div>
                                <div class="field-body">
                                    <div class="field is-expanded">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="mv_file_number" id="mv_file_number" value="{{ $mv_file_number }}">
                                        </p>
                                    </div>
                                    <div class="field is-expanded">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="plate_number" id="plate_number" placeholder="Plate Number" value="{{ $plate_number }}">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="engine_number">Engine Number</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded">
                                            <input class="input is-normal" type="text" name="engine_number" id="engine_number" value="{{ $engine_number }}">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="chasis_number">Chasis Number</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded">
                                            <input class="input is-normal" type="text" name="chasis_number" id="chasis_number" value="{{ $chasis_number }}">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="denomination">Denomination</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control is-expanded">
                                            <input class="input is-normal" type="text" name="denomination" id="denomination" value="{{ $denomination }}">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="number_of_cylinders">No. of Cylinders</label>
                                </div>
                                <div class="field-body">
                                    <div class="field is-expanded">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="number_of_cylinders" id="number_of_cylinders" value="{{ $number_of_cylinders }}">
                                        </p>
                                    </div>
                                    <div class="field is-expanded">
                                        <div class="control">
                                            <div class="select is-fullwidth">
                                                <select name="fuel_id" id="fuel_id">
                                                <option value="">FUEL</option>
                                                @foreach($fuels as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="make">Make</label>
                                </div>
                                <div class="field-body">
                                    <div class="field is-expanded">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="make" id="make" value="{{ $make }}">
                                        </p>
                                    </div>
                                    <div class="field is-expanded">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="series" id="series" value="{{ $series }}" placeholder="Series">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="body_type_id">Body Type</label>
                                </div>
                                <div class="field-body">
                                    <div class="field is-expanded">
                                        <div class="control">
                                            <div class="select is-fullwidth">
                                                <select name="body_type_id" id="body_type_id">
                                                <option value="">BODY TYPE</option>
                                                @foreach($body_types as $row)
                                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field is-expanded">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="body_number" id="body_number" value="{{ $body_number }}" placeholder="Body Number">
                                        </p>
                                    </div>
                                    <div class="field is-expanded">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="year_model" id="year_model" value="{{ $year_model }}" placeholder="Year Model">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label is-normal" for="driver_name">Owner</label>
                                </div>
                                <div class="field-body">
                                    <div class="field is-expanded">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="driver_name" id="driver_name" value="{{ $driver_name }}">
                                        </p>
                                    </div>
                                    <div class="field is-expanded">
                                        <p class="control">
                                            <input class="input is-normal" type="text" name="license_number" id="license_number" value="{{ $license_number }}" readonly="" placeholder="License Number">
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="field is-horizontal">
                                <div class="field-label"></div>
                                <div class="field-body">
                                    <div class="field">
                                        <div class="control">
                                            <button class="button is-info is-normal">Submit</button>
                                            {!! csrf_field() !!}
                                            {{ ((Request::segment(4) == 'edit')? method_field('PUT') : '') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection

@push('scripts')
<input type="hidden" id="json_search" value="{{ route('json_search') }}">

<script type="text/javascript">
    var xhr_driver_name = null;
    $('#fuel_id').val('{{ $fuel_id }}');
    $('#body_type_id').val('{{ $body_type_id }}');
    //$('#fuel_id option[value={{ $fuel_id or 0 }}]').attr('selected','selected');
    //$('#body_type_id option[value={{ $body_type_id or 0 }}]').attr('selected','selected');
    $(function(){
        $("#driver_name").autocomplete({
            source: function (request, response) 
            {
                xhr_driver_name = $.ajax({
                    type : 'get',
                    url : $("#json_search").val(),
                    data : 'search=' + request.term,
                    cache : true,
                    dataType : "json",
                    beforeSend: function(xhr) 
                    {
                        if (xhr_driver_name != null)
                        {
                            xhr_driver_name.abort();
                        }
                    }
                }).done(function(data) 
                {
                    response($.map( data, function(value, key) 
                    {
                        return { label: value, value: key }
                    }));

                }).fail(function(jqXHR, textStatus) 
                {
                    //console.log('Request failed: ' + textStatus);
                });
            }, 
            minLength: 2,
            autoFocus: true,
            select: function(event, ui)
            {
                var name = ui.item.label;
                var license_number = ui.item.value;

                $("#driver_name").val(name);
                $("#license_number").val(license_number);

                return false;
            }
        });
    });
</script>
@endpush